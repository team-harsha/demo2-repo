package com.test1.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.test1.demo.customer.Customer;
import com.test1.demo.repositories.CustomerRespository;

public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private CustomerRespository customerRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Customer customer = customerRepository.findByCustomerNumber(username);
		
		if(customer == null) {
			throw new UsernameNotFoundException("User doesn't exists!");
		}
		
		CustomUserDetails customUserDetails = new CustomUserDetails(customer);
		
		return customUserDetails;
	}

}
