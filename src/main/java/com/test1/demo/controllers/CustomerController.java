package com.test1.demo.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test1.demo.customer.Customer;
import com.test1.demo.repositories.EmployeeRespository;
import com.test1.demo.services.CustomerService;
import com.test1.demo.util.CustomerNumberGenerator;
import com.test1.demo.util.Jobs;
import com.test1.demo.util.LoginInput;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private EmployeeRespository employeeRepository;
	

	@Autowired
	private Jobs jobs;
	
	@Autowired
	private CustomerNumberGenerator customerNumberGenerator;

	final int SHORT_ID_LENGTH = 11;

	private static Logger logger = LogManager.getLogger(CustomerController.class);


	@GetMapping("")
	public String home() {

		logger.info("--------------------this is info----------------");
		return "index";
	}

	@GetMapping("/register")
	public String registerationForm(Model model) {

		Customer customer = new Customer();

		model.addAttribute("customer", customer);
		model.addAttribute("jobs", jobs.getAllJobs());
		logger.info("--------------------this is logger----------------");

		return "register";
	}

	@PostMapping("/register")
	public String registerUser(@Valid @ModelAttribute("customer") Customer customer, BindingResult bindingResult,
			Model model) {

		Customer existing = customerService.findByEmailId(customer.getEmailId());

		if (bindingResult.hasErrors() || customer.getSalesRepEmployeeNumber() == 0 || existing != null) {

			if (customer.getSalesRepEmployeeNumber() == 0) {
				bindingResult.rejectValue("salesRepEmployeeNumber", "error.register.salesRepEmployeeNumber",
						"Please select Employee Name");
			}
			if (existing != null) {
				bindingResult.rejectValue("emailId", "error.register.emailId",
						"Email already exists! Please choose another.");
			}

			model.addAttribute("jobs", jobs.getAllJobs());
			return "register";
		}

		else {

			
			
			customer.setCreditLimit(100000.00);
			customer.setUser_type(1);
			customer.setActive_status(1);
			
			String customerNo = customerNumberGenerator.generateCustomerNumber(customer.getCustomerName());
			
	        logger.info(customerNo);
	        customer.setCustomerNumber(customerNo);
	        
			Customer savedCustomer = customerService.save(customer);
			model.addAttribute("loginId", savedCustomer.getCustomerNumber());
			return "success";

		}

	}

	@ResponseBody
	@GetMapping("/loadEmpNo/{job}")
	public List<String> retrieveEmpNo(@PathVariable("job") String job) {

		List<String> empNo = employeeRepository.getEmpNo(job);
		return empNo;
	}

	@GetMapping("/login")
	public String loginForm(Model model) {
		LoginInput input = new LoginInput();
		model.addAttribute("loginInput", input);
		return "login";
	}

	@ResponseBody
	@PostMapping("/getSalesRepDetails")
	public List<String> handleLogin(@RequestParam Map<String, String> body) {
		
		
		String job = body.get("jobVal");
		logger.info(body);
		
		List<String> empNo = employeeRepository.getEmpNo(job);		
		
		logger.info(empNo);

		return empNo;

	}

	@GetMapping("homepage")
	public String homepage(Model model) {
		
		SecurityContext context = SecurityContextHolder.getContext();
		Customer existingCustomer = customerService.findByCustomerNumber(context.getAuthentication().getName());
		model.addAttribute("fullName", existingCustomer.getCustomerName());

		return "homepage";
	}

}
