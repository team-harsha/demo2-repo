package com.test1.demo.customer;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity(name = "customers")
@Table(name = "customers")
@SecondaryTable(name = "login_details",  pkJoinColumns=@PrimaryKeyJoinColumn(name="loginId", referencedColumnName="customerNumber"))
public class Customer {

	
	@Id
	@Column(name = "customerNumber", nullable = false, length = 11)
	@Size(min = 4, message = "Customer Name must contain min 4 characters!")
	private String customerNumber;
	
	@Column(name = "customerName", nullable = false, length = 50)
	@NotEmpty(message = "Customer Name cannot be empty")
	@Size(min = 2, message = "Customer Name must contain min 2 characters!")
	private String customerName; 
	
	@Column(name = "contactLastName", nullable = false, length = 50)
	@NotEmpty(message = "Last Name cannot be empty")
	@Size(min = 2, max = 12, message = "Last Name must contain 2-12 characters!")
	private String contactLastName; 
	
	@NotEmpty(message = "First Name cannot be empty")
	@Size(min = 2, max = 12, message = "First Name must contain 2-12 characters!")
	@Column(name = "contactFirstName", nullable = false, length = 50)
	private String contactFirstName;
	
	@NotEmpty(message = "Contact cannot be empty")
	@Size(min = 10, message = "Phone must contain min 10 characters!")
	@Column(name = "phone", nullable = false, length = 50)
	private String phone;  
	
	@NotEmpty(message = "Address Line 1 cannot be empty")
	@Column(name = "addressLine1", length = 50, nullable = false)
	private String addressLine1;  
		
	@Column(name = "addressLine2", length = 50)
	private String addressLine2;  
	
	@NotEmpty(message = "City cannot be empty")
	@Column(name = "city", nullable = false, length = 50)
	private String city;  
		
	@Column(name = "state", length = 50)
	private String state;  
	
	@NotEmpty(message = "Postal Code cannot be empty")
	@Column(name = "postalCode", nullable = false, length = 15)
	private String postalCode; 
	
	@NotEmpty(message = "Country cannot be empty")
	@Column(name = "country", nullable = false, length = 50)
	private String country;  
	
	@Column(name = "salesRepEmployeeNumber")
	private int salesRepEmployeeNumber;
	
	@NotNull(message = "Sales Rep Employee Number cannot be empty")
	@Column(name = "creditLimit")
	private double creditLimit;
	
	
	@Column(table = "login_details", name = "emailId")
	@NotNull(message = "Email cannot be empty")
	private String emailId;
	
	@Column(table = "login_details")
	@NotEmpty(message = "Password cannot be empty")
	@Size(min = 8)
	private String password;
	
	@Column(table = "login_details")
	private int user_type;
	
	@Column(table = "login_details")
	private int active_status;

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public int getUser_type() {
		return user_type;
	}

	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}

	public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public int getActive_status() {
		return active_status;
	}

	public void setActive_status(int active_status) {
		this.active_status = active_status;
	}
		
}
