package com.test1.demo.employee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity(name = "employees")
@Table(name = "employees")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int employeeNumber;
	
	@Column(name = "lastName", nullable = false, length = 50)
	@NotEmpty(message = "Last Name cannot be empty")
	@Size(min = 2, max = 12, message = "Last Name must contain 2-12 characters!")
	private String lastName; 
	
	@NotEmpty(message = "First Name cannot be empty")
	@Size(min = 2, max = 12, message = "First Name must contain 2-12 characters!")
	@Column(name = "firstName", nullable = false, length = 50)
	private String firstName;
	
	
	
	@Column(name = "extension", nullable = false, length = 10)
	private String extension;  
	
	@NotEmpty(message = "Email cannot be empty")
	@Column(name = "email", length = 100, nullable = false)
	private String email;  
		
	@Column(name = "officeCode", length = 10)
	private String officeCode;  
	
			
	@Column(name = "reportsTo", nullable = false)
	private int reportsTo;  
	
		
	@Column(name = "jobTitle", length = 50)
	private String jobTitle;
	

	@Override
	public String toString() {
		return "Employee [employeeNumber=" + employeeNumber + ", lastName=" + lastName + ", firstName=" + firstName
				+ ", extension=" + extension + ", email=" + email + ", officeCode=" + officeCode + ", reportsTo="
				+ reportsTo + ", jobTitle=" + jobTitle + "]";
	}


	public Employee() {
		
	}


	public int getEmployeeNumber() {
		return employeeNumber;
	}


	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getExtension() {
		return extension;
	}


	public void setExtension(String extension) {
		this.extension = extension;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getOfficeCode() {
		return officeCode;
	}


	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}


	public int getReportsTo() {
		return reportsTo;
	}


	public void setReportsTo(int reportsTo) {
		this.reportsTo = reportsTo;
	}


	public String getJobTitle() {
		return jobTitle;
	}


	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}


	
	

	

	
}
