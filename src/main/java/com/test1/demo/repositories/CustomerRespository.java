package com.test1.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.test1.demo.customer.Customer;

@Repository
public interface CustomerRespository extends JpaRepository<Customer, Integer> {

	public Customer findByCustomerNumber(String id);

	public Customer findByEmailId(String email);

}
