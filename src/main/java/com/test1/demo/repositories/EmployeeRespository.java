package com.test1.demo.repositories;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.test1.demo.employee.Employee;

@Repository
public interface EmployeeRespository extends JpaRepository<Employee, Integer> {
	
	
	  @Query("SELECT DISTINCT jobTitle FROM employees") 
	  public List<String> getJobs();

	  @Query("SELECT employeeNumber,firstName,lastName FROM employees WHERE jobTitle=:job") 
	  public List<String> getEmpNo(@Param("job") String job);
	  
	  
	  
	  
}
