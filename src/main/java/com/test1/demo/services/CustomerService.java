package com.test1.demo.services;

import com.test1.demo.customer.Customer;

public interface CustomerService {
	
	Customer save(Customer customer);
	
	Customer findByCustomerNumber(String id);
	
	Customer findByEmailId(String email);
	

}
