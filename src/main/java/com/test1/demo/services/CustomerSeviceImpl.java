package com.test1.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.test1.demo.customer.Customer;


@Service
public class CustomerSeviceImpl implements CustomerService {

	@Autowired
	private com.test1.demo.repositories.CustomerRespository customerRespository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public Customer save(Customer customer) {
		customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
		return customerRespository.save(customer);
	}

	@Override
	public Customer findByCustomerNumber(String id) {
		
		return customerRespository.findByCustomerNumber(id);
	}

	@Override
	public Customer findByEmailId(String email) {
		
		return customerRespository.findByEmailId(email);
	}

	

	

	

}
