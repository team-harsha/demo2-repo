package com.test1.demo.util;

import org.springframework.stereotype.Component;

@Component
public class CustomerNumberGenerator {
	
	public String generateCustomerNumber(String name) {
		
		long time = System.currentTimeMillis();
		
		String generatedName = name.replaceAll(" ", "@");
        String customerNo = generatedName.substring(0, 3) + String.valueOf(time).substring(3,11);
		return customerNo;
	}

}
