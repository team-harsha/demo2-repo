package com.test1.demo.util;


import org.springframework.stereotype.Component;

@Component
public class JobValue {

	private String jobValue;

	
	public JobValue(String jobValue) {
		super();
		this.jobValue = jobValue;
	}
	
	

	public JobValue() {
		super();
	}



	public String getJobValue() {
		return jobValue;
	}

	public void setJobValue(String jobValue) {
		this.jobValue = jobValue;
	}
	
	
}
