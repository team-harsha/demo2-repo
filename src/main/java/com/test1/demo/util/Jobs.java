package com.test1.demo.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test1.demo.repositories.EmployeeRespository;



@Component
public class Jobs {
	
	@Autowired
	private EmployeeRespository employeeRespository;
	
	
	public List<String> getAllJobs(){
		return employeeRespository.getJobs();
	}

}
