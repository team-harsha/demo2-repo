package com.test1.demo.util;

public class LoginInput {

	private String loginId;
	private String password;
	
	
	public LoginInput() {
		super();
	}
	public LoginInput(String loginId, String password) {
		super();
		this.loginId = loginId;
		this.password = password;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "LoginInput [loginId=" + loginId + ", password=" + password + "]";
	}
		
}
