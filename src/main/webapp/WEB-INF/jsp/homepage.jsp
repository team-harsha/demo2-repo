<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/indexStyle.css">
	
	<meta charset="ISO-8859-1">
	<title>Homepage</title>
</head>
<body>

<header class="header">
	<h1 class="heading">Mahindra First Choice Wheels</h1>
</header>
	
	<div class="container">
	<div class="d-flex justify-content-center ">
		<div class="card">
			<div class="card-header">
				<h3>Dashboard</h3>
				<div class="d-flex justify-content-end social_icon">
					<a href="https://www.facebook.com/MahindraFirstChoiceWheels/" target="_blank">
						<span><i class="fab fa-facebook-square"></i></span>
					</a>
					<a href="https://www.instagram.com/mahindrafirstchoice/?hl=en" target="_blank">
						<span><i class="fab fa-instagram"></i></span>
					</a>
					<a href="https://twitter.com/MFCWL?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank">
						<span><i class="fab fa-twitter-square"></i></span>
					</a>
				</div>
			</div>
			<div class="card-body">
			<div align="center">
				<h3 style="color: white;">Welcome ${fullName}</h3>
				<form name='logout' action="/logout" method='POST'>
        
        			<button type="submit" class="btn btn-danger m-2 w-100">Logout</button>
        
    			</form>
			</div>
		</div>				
	</div>
			
		</div>
	</div>


</body>
</html>