<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    

<!DOCTYPE html>
<html>

 <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">


<head>
<meta charset="ISO-8859-1">
	<title>Login Page</title>
   
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="/loginStyle.css">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-end h-100">
		<div class="card">
			<div class="card-header">
				<h3>Log In</h3>
				<div class="d-flex justify-content-end social_icon">
				<a href="https://www.facebook.com/MahindraFirstChoiceWheels/" target="_blank">
					<span><i class="fab fa-facebook-square"></i></span></a>
					
					<a href="https://www.instagram.com/mahindrafirstchoice/?hl=en" target="_blank">
						<span><i class="fab fa-instagram"></i></span>
					</a>
					<a href="https://twitter.com/MFCWL?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank">
					<span><i class="fab fa-twitter-square"></i></span></a>
				</div>
			</div>
			<div class="card-body">
			<p>
					<a href="/" class="text-light"> < Back to home</a>
				</p>
			<c:if test="${param.logout != null}">         
		        <div class="logout">  
		            You have been logged out successfully. 
		        </div>  
    		</c:if>  
				<form name='login' action="/login" method='POST'>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="username" name="username" required="required">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" placeholder="password" name="password" required="required">
					</div>
					
					<c:if test="${param.error != null}">
					
													
						<div class="error mb-2" >
							
							${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
						</div>
					</c:if>
					
					
					
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="register">Sign Up</a>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
	console.log(param);
</script>
</body>
</html>