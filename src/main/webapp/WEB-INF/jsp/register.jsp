<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Registration Page</title>
   
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="/registerStyle.css">
</head>
<meta charset="ISO-8859-1"><body>
<div class="container">
	<div class="d-flex justify-content-end h-100">
		<div class="card">
			<div class="card-header">
				<h3>Register</h3>
				<div class="d-flex justify-content-end social_icon">
					<a href="https://www.facebook.com/MahindraFirstChoiceWheels/" target="_blank">
						<span><i class="fab fa-facebook-square"></i></span>
					</a>
					<a href="https://www.instagram.com/mahindrafirstchoice/?hl=en" target="_blank">
						<span><i class="fab fa-instagram"></i></span>
					</a>
					<a href="https://twitter.com/MFCWL?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank">
						<span><i class="fab fa-twitter-square"></i></span>
					</a>
				</div>
			</div>
			<div class="card-body">
				<p>
					<a href="/" class="text-light"> < Back to home</a>
				</p>
				<form:form action="register" method='POST' modelAttribute="customer" onsubmit="return validateForm()">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Customer Name</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Customer Name"
								required="required" path="customerName" minlength="4" />
						<form:errors path="customerName" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>First Name</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter First Name"
								required="required" path="contactFirstName" minlength="2" />
						<form:errors path="contactFirstName" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Last Name</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter Last Name"
								required="required" path="contactLastName" minlength="2" />
						<form:errors path="contactLastName" cssClass="error" />
					</div>
			<div >
					<div class="input-group form-group">
					
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Phone</i>
							</span>
						</div>
						
						<form:input type="number" class="form-control" placeholder="Enter Phone"
								required="required" path="phone" minlength="10" />
				</div>
								<div class="mb-1">
						<form:errors path="phone" cssClass="error" />
						</div>
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Email</i>
							</span>
						</div>
						<form:input type="email" class="form-control" placeholder="Enter Email"
								required="required" path="emailId" />
						<form:errors path="emailId" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Address Line 1</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter Address Line 1"
								required="required" path="addressLine1" />
						<form:errors path="addressLine1" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Address Line 2</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter Address Line 2"
								 path="addressLine2" />
						<form:errors path="addressLine2" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>City</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter City"
								 path="city" required="required" />
						<form:errors path="city" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>State</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter State"
								 path="state" />
						<form:errors path="state" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Postal Code</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter Postal Code"
								 path="postalCode" />
						<form:errors path="postalCode" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Country</i>
							</span>
						</div>
						<form:input type="text" class="form-control" placeholder="Enter Country"
								 path="country" required="required" />
						<form:errors path="country" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Password</i>
							</span>
						</div>
						<form:input type="password" class="form-control" placeholder="Enter Password"
								 path="password" required="required" minlength="8" />
						<form:errors path="password" cssClass="error" />
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Job Titles</i>
							</span>
						</div>
						<select id="jobTitle" name="jobTitles" class="form-control">
							<c:forEach var="jobs" items="${jobs}">
								<option value="" selected disabled hidden="true">--Select Job--</option>
								<option value="${jobs}">${jobs}</option>
							</c:forEach>
					</select>
					</div>
					
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i>Sales Rep Employee Name</i>
							</span>
						</div>
							<form:select id="salesRepEmployeeNumber" type="number" path="salesRepEmployeeNumber" class="form-control">
								<option selected disabled hidden="true">--Select Employee Name--</option>
							</form:select>
							<form:errors path="salesRepEmployeeNumber" cssClass="error" />
					</div>				
					
					<div class="form-group" align="right">
						<input type="submit" value="Register"
								class="btn register_btn">
					</div>
				</form:form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Already have an account?<a href="login">Login</a>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">

	function validateForm(){
		console.log("validate form ran");
		const sNo = document.getElementById("salesRepEmployeeNumber").value;
		const job = document.getElementById("jobTitle").value;
		console.log(job);
		console.log(sNo);
		if(job === ""){
			alert("Please Select Job Title");
			return false;
		}
		if(sNo === "--Select Employee Name--"){
			alert("Please Select Sales Rep Employee Name");
			return false;
		}
		
		
		return true;
	}

	 $(document).ready(function () {
      
      /* $("#jobTitle").on("change", function () {
        var job = $(this).val();
        
         $.ajax({
          type: "GET",
          url: "/loadEmpNo/" + job,
          success: function (result) {
        
              var state = "";
                        
            result.forEach(item => {
            	const arr = item.split(",");
            	state += "<option disabled hidden='true' selected='selected'>--Select Employee Name--</option>" ;
            	state +=
                    '<option value="' +
                    arr[0] +
                    '">' +
                    arr[1] + " " + arr[2]
                    "</option>";
            })
             $("#salesRepEmployeeNumber").html(state);
           
          },
        }); */
         
         
         
         $("#jobTitle").on("change", function () {
             var job = $(this).val() + "";
             console.log(job);
             
              $.ajax({
               type: "POST",
               url: "/getSalesRepDetails",
               data: {jobVal: job},
              /*  dataType: "application/json", */
               success: function (result) {
            	  
                   var state = "";
                             
                result.forEach(item => {
                 	const arr = item.split(",");
                 	state += "<option disabled hidden='true' selected='selected'>--Select Employee Name--</option>" ;
                 	state +=
                         '<option value="' +
                         arr[0] +
                         '">' +
                         arr[1] + " " + arr[2]
                         "</option>";
                 })
                  $("#salesRepEmployeeNumber").html(state);
                
               },
             });
         
         
         
         
            
      });

    /*  });    */
	});
     
  </script>
</body>
</html>